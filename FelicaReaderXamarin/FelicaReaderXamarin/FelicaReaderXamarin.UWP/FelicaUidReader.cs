﻿using Felica;
using FelicaReaderXamarin.UWP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.SmartCards;

[assembly: Xamarin.Forms.Dependency(typeof(FelicaUidReader))]
namespace FelicaReaderXamarin.UWP
{
    class FelicaUidReader : IFelicaUidReader
    {
        public async Task<string> PollFelicaUid()
        {
            var selector = SmartCardReader.GetDeviceSelector(SmartCardReaderKind.Any);
            var devices = await DeviceInformation.FindAllAsync(selector);
            var device = devices.FirstOrDefault();
            if (device == null) {
                return null;
            }

            var reader = await SmartCardReader.FromIdAsync(device.Id);
            if (reader == null) {
                return null;
            }

            // カード検索
            var cards = await reader.FindAllCardsAsync();
            var card = cards.FirstOrDefault();
            if (card == null) {
                return null;
            }

            // 接続してポーリングコマンド送信
            using (var con = await card.ConnectAsync())
            {
                var handler = new AccessHandler(con);
                var uid = await handler.GetUidAsync();
                return BitConverter.ToString(uid);
            }
        }
    }
}
