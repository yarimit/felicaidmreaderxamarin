﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FelicaReaderXamarin
{
    public partial class MainPage : ContentPage
    {
        public class MainPageViewModel : Monitorable {
            public string Uid_;
            public string Uid {
                get {
                    return Uid_;
                }
                set {
                    Uid_ = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private MainPageViewModel ViewModel { get; set; }
        private CancellationTokenSource cts_;

        public MainPage()
        {
            InitializeComponent();
            this.ViewModel = new MainPageViewModel();
            this.BindingContext = this.ViewModel;

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            cts_ = new CancellationTokenSource();
            Task.Run(async () => {
                try
                {
                    IFelicaUidReader reader = DependencyService.Get<IFelicaUidReader>();

                    while (true)
                    {
                        await Task.Delay(200);
                        string uid = await reader.PollFelicaUid();

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            this.ViewModel.Uid = uid ?? "";
                        });
                    }
                }
                catch (OperationCanceledException)
                {
                    Debug.WriteLine("Polling Task Cancelled");
                }

                cts_ = null;
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            if(cts_ != null)
            {
                cts_.Cancel();

            }
        }

    }
}
